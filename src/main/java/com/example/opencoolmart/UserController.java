package com.example.opencoolmart;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UserController {
    @Autowired
    private UserRepository _userRepository;

    @PostMapping("/add")
    public String addUser(@RequestParam String password,@RequestParam String email){
        User user = new User();
        user.setPassword(password);
        user.setEmail(email);
        _userRepository.save(user);
        return "Add new User to repo";
    }
    @PutMapping("/update/{id}")
    public String updateUser(@RequestBody User newUser,@PathVariable Integer id){
        User user = _userRepository.findById(id).map(userOld -> {
            userOld.setEmail(newUser.getEmail());
            userOld.setPassword(newUser.getPassword());
            return _userRepository.save(userOld);
        }).orElseGet(()-> {
            newUser.setId(id);
            return _userRepository.save(newUser);
        });
        return "Update User in to repo";
    }
    @GetMapping("/list")
    public Iterable<User> getUsers(){return _userRepository.findAll();}
    @GetMapping("/find/{id}")
    public User findUserById(@PathVariable Integer id){
        return _userRepository.findUserById(id);
    }
}
